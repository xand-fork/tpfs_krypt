//TODO: These do not function without a working vault server
// #![forbid(unsafe_code)]

// #[cfg(all(test, feature = "hashicorp-vault"))]
// mod common;

// #[cfg(feature = "hashicorp-vault")]
// mod vault_integ {
//     use crate::common::*;
//     use proptest::prelude::*;
//     use regex_generate::{Generator, DEFAULT_MAX_REPEAT};
//     use std::cell::RefCell;
//     use strum::IntoEnumIterator;
//     use tpfs_krypt::{
//         config::{KeyManagerConfig, KryptConfig},
//         errors::KeyManagementError,
//         from_config,
//         hashicorp_vault_key_manager::*,
//         KeyIdentifier, KeyManagement, KeyType,
//     };
//     use url::Url;

//     thread_local! {
//         static PATH_GENERATOR: RefCell<Generator<rand::rngs::ThreadRng>> =
//             RefCell::new(
//                 Generator::new(
//                     "[A-Za-z0-9]{20, 25}",
//                     rand::thread_rng(),
//                     DEFAULT_MAX_REPEAT,
//                 ).unwrap()
//             );
//     }

//     fn generate_random_path_name() -> String {
//         let mut buffer = vec![];
//         PATH_GENERATOR.with(|g| g.borrow_mut().generate(&mut buffer).unwrap());
//         String::from_utf8(buffer).unwrap()
//     }

//     fn create_vault_config() -> HashicorpVaultKeyManagerConfig {
//         let vault_path = format!("integ_tests/keypairs/{}/", generate_random_path_name());
//         let vault_token = std::env::var("VAULT_TOKEN").unwrap_or_else(|_| "testtoken".into());
//         let vault_addr =
//             std::env::var("VAULT_ADDR").unwrap_or_else(|_| "http://127.0.0.1:8200".into());
//         let vault_addr = Url::parse(&vault_addr).unwrap();

//         HashicorpVaultKeyManagerConfig {
//             vault_path,
//             vault_token,
//             vault_addr,
//         }
//     }

//     fn create_key_manager() -> Box<dyn KeyManagement> {
//         let config = create_vault_config();
//         let krypt_config = KryptConfig {
//             key_manager_config: KeyManagerConfig::HashicorpVaultKeyManager(config),
//         };

//         from_config(krypt_config).unwrap()
//     }

//     fn add_keypairs(
//         count: u8,
//         key_type: KeyType,
//         key_manager: &mut Box<dyn KeyManagement>,
//     ) -> Vec<String> {
//         std::iter::repeat_with(|| key_manager.generate_keypair(key_type).unwrap().id.value)
//             .take(count as usize)
//             .collect()
//     }

//     #[test]
//     fn valid_config_passes() {
//         let manager_config = create_vault_config();
//         let krypt_config = KryptConfig {
//             key_manager_config: KeyManagerConfig::HashicorpVaultKeyManager(manager_config),
//         };

//         let result = from_config(krypt_config);

//         assert!(result.is_ok());
//     }

//     #[test]
//     fn get_addresses_empty_initially() {
//         let sut = create_key_manager();

//         let addresses = sut.get_key_ids().unwrap();

//         assert_eq!(0, addresses.len());
//     }

//     proptest! {
//         #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
//         #[test]
//         fn addresses_generate_counts(key_type in keytype_strategy()) {
//             let mut sut = create_key_manager();
//             const TEST_COUNT: u8 = 4;
//             let mut expected_addresses = add_keypairs(TEST_COUNT, key_type, &mut sut);
//             expected_addresses.sort();

//             let addresses = sut.get_key_ids().unwrap();
//             let mut addresses: Vec<String> = addresses
//                 .into_iter()
//                 .map(|address| address.value)
//                 .collect();
//             addresses.sort();

//             prop_assert_eq!(addresses.len() as u8, TEST_COUNT);
//             prop_assert_eq!(&expected_addresses, &addresses);
//         }
//     }

//     proptest! {
//         #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
//         #[test]
//         fn has_key(key_type in keytype_strategy()) {
//             let mut sut = create_key_manager();
//             let mut other_key_manager = create_in_memory_key_manager();
//             let in_manager_generated = sut.generate_keypair(key_type).unwrap();
//             let generated = other_key_manager.generate_keypair(key_type).unwrap();

//             let in_manager_result = sut.has_key(in_manager_generated.as_ref()).unwrap();
//             let generated_result = sut.has_key(generated.as_ref()).unwrap();

//             prop_assert!(in_manager_result);
//             prop_assert!(!generated_result);
//         }
//     }

//     proptest! {
//         #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
//         #[test]
//         fn import_address(key_type in keytype_strategy()) {
//             let mut sut = create_key_manager();
//             let generated_phrase = generate_secret_phrase(&key_type);
//             let generated_address = get_address_from_secret_phrase(&generated_phrase, &key_type);
//             let addresses = sut.get_key_ids().unwrap();
//             prop_assert_eq!(0, addresses.len());

//             sut.import_keypair(generated_phrase, key_type).unwrap();
//             let addresses = sut.get_key_ids().unwrap();

//             prop_assert_eq!(addresses.len() as u8, 1);
//             prop_assert_eq!(&generated_address, &addresses[0].value);
//         }
//     }

//     proptest! {
//         // Doesn't actually verify the signature since those tests
//         // exist in the crypto module.
//         #![proptest_config(ProptestConfig::with_cases(20))]
//         #[test]
//         fn signing_occurs_from_hashi_key_manager_interface(
//             key_type in sign_capable_keytype_strategy(),
//             message in proptest::string::bytes_regex(".+").unwrap()
//         ) {
//             let mut sut = create_key_manager();
//             let generated_address = sut.generate_keypair(key_type).unwrap();

//             let signed = sut.sign(generated_address.as_ref(), message.as_slice()).unwrap();
//             prop_assert!(!signed.as_ref().as_ref().is_empty());
//         }
//     }

//     proptest! {
//         #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
//         #[test]
//         fn sign_invalid_address(key_type in keytype_strategy()) {
//             let sut = create_key_manager();
//             let invalid_address = KeyIdentifier {
//                 value: "../../5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ".into(),
//                 key_type
//             };

//             let result = sut.sign(&invalid_address, b"Some Message");

//             prop_assert!(result.is_err());
//         }
//     }

//     #[test]
//     fn sign_non_existing_key() {
//         let sut = create_key_manager();
//         let generated_phrase = generate_secret_phrase(&KeyType::SubstrateSr25519);
//         let generated_address =
//             get_address_from_secret_phrase(&generated_phrase, &KeyType::SubstrateSr25519);
//         let generated_address = KeyIdentifier {
//             value: generated_address,
//             key_type: KeyType::SubstrateSr25519,
//         };

//         let result = sut.sign(&generated_address, b"A Message that won't be signed");

//         match result {
//             Err(KeyManagementError::AddressNotFound { address }) => {
//                 assert_eq!(&generated_address.value, &address)
//             }
//             _ => panic!(
//                 "Expected an AddressNotFound for: {}",
//                 &generated_address.value
//             ),
//         }
//     }
// }
