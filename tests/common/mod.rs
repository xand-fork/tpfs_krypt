use base58::ToBase58;
use bip39::{Language, Mnemonic, MnemonicType};
use proptest::prelude::*;
use secrecy::{ExposeSecret, Secret};
use sp_core::{crypto::Ss58Codec, ed25519, sr25519, Pair};
use strum::IntoEnumIterator;
use tpfs_krypt::{
    config::{KeyManagerConfig, KryptConfig},
    from_config,
    in_memory_key_manager::InMemoryKeyManagerConfig,
    KeyManagement, KeyType,
};

pub(crate) fn sign_capable_keytype_strategy() -> BoxedStrategy<KeyType> {
    let justs: Vec<Just<KeyType>> = KeyType::iter()
        // Shared encryption does not support signing
        .filter(|x| *x != KeyType::SharedEncryptionX25519)
        .map(Just)
        .collect();
    union_justs(justs)
}

pub(crate) fn keytype_strategy() -> BoxedStrategy<KeyType> {
    let justs: Vec<Just<KeyType>> = KeyType::iter().map(Just).collect();
    union_justs(justs)
}

fn union_justs(justs: Vec<Just<KeyType>>) -> BoxedStrategy<KeyType> {
    if justs.len() > 1 {
        let mut strategy = justs[0].prop_union(justs[1]);
        for just in justs.iter().skip(2) {
            strategy = strategy.or(*just);
        }
        return strategy.boxed();
    }
    panic!("There should be more than 1 KeyType.");
}

pub(crate) fn create_in_memory_key_manager() -> Box<dyn KeyManagement> {
    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::InMemoryKeyManager(InMemoryKeyManagerConfig {
            initial_keys: vec![],
        }),
    };

    from_config(config).unwrap()
}

pub(crate) fn generate_secret_phrase(key_type: &KeyType) -> Secret<String> {
    match key_type {
        KeyType::SubstrateSr25519 => {
            let (_, phrase, _) = sr25519::Pair::generate_with_phrase(None);

            Secret::new(phrase)
        }
        KeyType::SubstrateEd25519 => {
            let (_, phrase, _) = ed25519::Pair::generate_with_phrase(None);

            Secret::new(phrase)
        }
        KeyType::SharedEncryptionX25519 => {
            let mnemonic = Mnemonic::new(MnemonicType::Words24, Language::English);
            Secret::new(mnemonic.phrase().to_string())
        }
    }
}

pub(crate) fn get_address_from_secret_phrase(
    secret_phrase: &Secret<String>,
    key_type: &KeyType,
) -> String {
    match key_type {
        KeyType::SubstrateSr25519 => {
            let (pair, _) =
                sr25519::Pair::from_phrase(secret_phrase.expose_secret(), None).unwrap();

            pair.public().to_ss58check()
        }
        KeyType::SubstrateEd25519 => {
            let (pair, _) =
                ed25519::Pair::from_phrase(secret_phrase.expose_secret(), None).unwrap();

            pair.public().to_ss58check()
        }
        KeyType::SharedEncryptionX25519 => {
            let mnemonic = bip39::Mnemonic::from_phrase(
                secret_phrase.expose_secret(),
                bip39::Language::English,
            )
            .unwrap();
            let entropy = mnemonic.entropy();
            let mut private_key = [0u8; 32];
            private_key.copy_from_slice(&entropy[..32]);

            let secret: x25519_dalek::StaticSecret = private_key.into();
            let public_key: x25519_dalek::PublicKey = (&secret).into();

            public_key.as_bytes().to_base58()
        }
    }
}
