<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Design Proposal for tpfs krypt](#design-proposal-for-tpfs-krypt)
  - [Purpose](#purpose)
  - [Class Diagrams](#class-diagrams)
  - [Working Theories for What Code Will Look Like](#working-theories-for-what-code-will-look-like)
  - [TODOS](#todos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Design Proposal for tpfs krypt

## Purpose
TpfsKrypt provides an implementation-agnostic interface used to manage (e.g. generate, list or import keypairs) and work with (e.g. sign and later encrypt) cryptographic secrets.  Note that Network participants will employ different technologies to manage their cryptographic secrets they will use with XAND and TpfsKrypt will enable interfacing to these various solutions in a consistent manner.

Additionally it can provide a way to perform Secret Management. This proposal will help highlight an initial implementation which will be used with `xand-api` and potentially with `xand-submiter` and/or `xand-proposals`.

In order to keep the private keys or secrets safe after usage within the application. This implementation is assuming the use of the secrecy crate when returning secrets to help with the following.
> which attempts to limit accidental exposure and ensure secrets are wiped from memory when dropped.
https://docs.rs/secrecy/0.6.0/secrecy/

## Class Diagrams

Class diagrams for mermaid aren't working with gitlab markdown due to svg not working from mermaid.\
So using png formats in it's place:\
![key management](keymanagement.png)


![key management config](keymanagementconfig.png)

```
classDiagram
class KeyManagement
KeyManagement ..> Address
Address : +value String
Address : +key_type KeyType

KeyManagement <|-- FileKeyManager
KeyManagement <|-- VaultKeyManager
KeyManagement <|-- HSMKeyManager
KeyManagement : +get_addresses(&self) Result&lt;Vec&lt;Address&gt;&gt;
KeyManagement : +generate_keypair(&self, key_type: KeyType) Result&lt;Address&gt;
KeyManagement : +import_keypair(&self, public_key: impl AsRef&lt;u8&gt;, private_key: Secret&lt;impl AsRef&lt;u8&gt; + Zeroize&gt;, key_type: KeyType) Result&lt;Address&gt;
KeyManagement : +sign(&self, address: Address, message: &[u8]) Result&lt;Box&lt;dyn Signature&gt;&gt;

class Config
Config <| -- FileKeyManagerConfig
Config <| -- VaultKeyManagerConfig
Config <| -- HSMKeyManagerConfig

FileKeyManagerConfig : filePath &str
VaultKeyManagerConfig : vault_path &str
VaultKeyManagerConfig : vault_token &str
VaultKeyManagerConfig : vault_addr &str
HSMKeyManagerConfig : pkcs11_module_path &str
```

## Working Theories for What Code Will Look Like

```rust
/// The different sorts of keys that will be managed via this crate.
/// This will be persisted with the key for when we may want to be able
/// to return the type of the key after it's been imported or generated.
#[derive(
    Copy, Clone, Debug, PartialEq, EnumString, EnumIter, Display, Deserialize, Serialize, Zeroize,
)]
pub enum KeyType {
    #[cfg(feature = "substrate")]
    SubstrateSr25519,
    #[cfg(feature = "substrate")]
    SubstrateEd25519,
}

pub struct Address {
    pub value: String,
    pub key_type: KeyType,
}

/// The trait for KeyManagement that organizes based on addresses
/// created from the codec based on the ketype.
pub trait KeyManagement {
    /// Retrieves all the addresses that this KeyManager holds.
    fn get_addresses(&self) -> Result<Vec<Address>>;

    /// Generates a new keypair based on the KeyType supplied.
    /// This may take configuration to say how to generate a keypair.
    /// Which may include using derived types.
    fn generate_keypair(&self, key_type: KeyType) -> Result<Address>;

    /// Allows for saving a keypair that may have been created
    /// outside of the keymanager and save it with the KeyManager.
    fn import_keypair(
        &self,
        public_key: impl AsRef<[u8]>,
        private_key: Secret<impl AsRef<[u8]> + Zeroize>,
        key_type: KeyType,
    ) -> Result<Address>;

    /// Signs the message using the keypair associated with the provided address, if found.
    fn sign(&self, address: Address, message: &[u8]) -> Result<Box<dyn Signature>>;
}

/// A trait for the signature that may be expanded on later.
pub trait Signature: AsRef<[u8]> {}
```
