<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Tpfs Krypt](#tpfs-krypt)
- [Initial Design Proposal](#initial-design-proposal)
- [Rust Docs](#rust-docs)
- [Working with this crate](#working-with-this-crate)
  - [Specifying which KeyManager](#specifying-which-keymanager)
  - [KeyManager Interface](#keymanager-interface)
  - [Example: Substrate DEV_PHRASE with derived keys.](#example-substrate-dev_phrase-with-derived-keys)
  - [Example: For use in Tests](#example-for-use-in-tests)
- [Contributing](#contributing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Tpfs Krypt
TpfsKrypt provides an implementation-agnostic interface used to manage (e.g. generate, list or import keypairs) and work with (e.g. sign and later encrypt) cryptographic secrets.  Note that Network participants will employ different technologies to manage their cryptographic secrets they will use with XAND and TpfsKrypt will enable interfacing to these various solutions in a consistent manner.

Potential Implementations of KeyManagement that exist or may exist in the future are:
* Local File System
* Local pkcs11 compatible device (e.g. Hardware Security Module)
* Cloud Provided Security Key Management
* Key Value Secure Storage like Vault
* KMIP
* An Http Request Protocol (allow a completely customized solution)
* A GRPC Protocol (allow a completely customized solution)

# Initial Design Proposal
Included in this repo is what the initial design proposal of what TpfsKrypt could look like. See the [design proposal document](original_proposal/DESIGN_PROPOSAL.md).

# Rust Docs

There are hosted rust docs if you would find those easier to work with at the gitlab pages url of:
https://transparentincdevelopment.gitlab.io/product/libs/tpfs_krypt/tpfs_krypt/index.html

# Working with this crate

Below are what it would look like to work with a KeyManager to manage keys and sign messages.
More details can be found in the Rust Docs.

## KeyManager Interface

This is the trait within [lib.rs](src/lib.rs) see [rust docs](https://transparentincdevelopment.gitlab.io/product/libs/tpfs_krypt/tpfs_krypt/index.html) . This is mostly what you would work with from this crate.

Here's a basic example for what it looks like to work with the library:

```rust
use std::{path::PathBuf, fs};
use tpfs_krypt::{
    config::{KeyManagerConfig, KryptConfig},
    errors::KeyManagementError,
    from_config, FileKeyManagerConfig, KeyType, secrecy::Secret,
    sp_core::{crypto::{DEV_PHRASE, Pair, Ss58Codec}, sr25519}
};

let path = PathBuf::from("/tmp/krypt/keypairs/");
if !path.exists() {
    fs::create_dir_all(&path).unwrap();
}

let config = KryptConfig {
    key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
        keypair_directory_path: path.into_os_string().into_string().unwrap(),
    }),
};

let mut key_manager = from_config(config)?;
let secret_phrase = Secret::new(format!("{}//Xand", DEV_PHRASE));
let address = key_manager.import_keypair(secret_phrase, KeyType::SubstrateSr25519)?;
let signature = key_manager.sign(address.as_ref(), b"My important message that can be verified was done by me via my public address.")?;
let signature_bytes: &[u8] = signature.as_ref().as_ref();

// You can generate the address yourself with the code below.
let pair = sr25519::Pair::from_string("//Xand", None)?;
assert_eq!(pair.public().to_ss58check(), address.id.value);
```

## Specifying which KeyManager

Initialize the type of KeyManager that you are working with. This can be done via one of two ways:
* Config file named krypt.toml at a specified path along with optional environment variables prefixed with KRYPT
* Using the config structs directly.

You'll find an example of working with the file key manager here, but others can be found in the examples directory.

```toml
[key_manager_config.FileKeyManager]
keypair_directory_path = "/etc/keypairs"
```

Let's say that was saved as `/etc/xand-api/krypt.toml` Then to crete a KeyManager would be:
```rust
use tpfs_krypt::{from_config_path, KeyType};

let key_manager = from_config_path("/etc/xand-api/");
let has_xand_key = key_manager.has_address("5Cqm7KKdm8MB7jR66mxKKcUAzKGFbZnYJqMvQQBpgC5P9C2W")?;
```

Example config files can be found in the [examples](examples) folder.

# Contributing

See the [contributing document](CONTRIBUTING.md) for how to work within this crate.
