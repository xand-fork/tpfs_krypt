use super::*;
use crate::unit_tests::prelude::*;
use crate::{KeyManagement, Signing};
use proptest::prelude::*;
use strum::IntoEnumIterator;

fn create_in_memory_key_manager() -> InMemoryKeyManager {
    let config = InMemoryKeyManagerConfig {
        initial_keys: vec![],
    };

    InMemoryKeyManager::new(config)
}

fn add_keypairs(count: u8, key_type: KeyType, key_manager: &mut InMemoryKeyManager) -> Vec<String> {
    std::iter::repeat_with(|| key_manager.generate_keypair(key_type).unwrap().id.value)
        .take(count as usize)
        .collect()
}

#[test]
fn valid_config_passes() {
    let manager_config = InMemoryKeyManagerConfig {
        initial_keys: vec![],
    };

    let result = manager_config.validate();

    assert!(result.is_ok());
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn addresses_generate_counts(key_type in keytype_strategy()) {
        let mut sut = create_in_memory_key_manager();
        const TEST_COUNT: u8 = 4;
        let mut expected_addresses = add_keypairs(TEST_COUNT, key_type, &mut sut);
        expected_addresses.sort();

        let addresses = sut.get_key_ids().unwrap();
        let mut addresses: Vec<String> = addresses
            .into_iter()
            .map(|address| address.value)
            .collect();
        addresses.sort();

        prop_assert_eq!(addresses.len() as u8, TEST_COUNT);
        prop_assert_eq!(&expected_addresses, &addresses);
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn has_key(key_type in keytype_strategy()) {
        let mut sut = create_in_memory_key_manager();
        let in_manager_generated = sut.generate_keypair(key_type).unwrap();
        let generated = KeyPairInstance::generate(key_type).unwrap();

        let in_manager_result = sut.has_key(in_manager_generated.as_ref()).unwrap();
        let generated_result = sut.has_key(&generated.identifier()).unwrap();

        prop_assert!(in_manager_result);
        prop_assert!(!generated_result);
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn import_address(key_type in keytype_strategy()) {
        let mut sut = create_in_memory_key_manager();
        let generated = KeyPairInstance::generate(key_type).unwrap();
        let addresses = sut.get_key_ids().unwrap();
        prop_assert_eq!(0, addresses.len());

        sut.import_keypair(generated.to_secret().unwrap(), key_type).unwrap();
        let addresses = sut.get_key_ids().unwrap();

        prop_assert_eq!(addresses.len() as u8, 1);
        prop_assert_eq!(generated.identifier(), addresses[0].clone());
    }
}

#[test]
fn sign_non_existing_key() {
    let sut = create_in_memory_key_manager();
    let generated = KeyPairInstance::generate(KeyType::SubstrateSr25519).unwrap();
    let generated_address = generated.identifier();

    let result = sut.sign(&generated_address, b"A Message that won't be signed");

    match result {
        Err(KeyManagementError::AddressNotFound { address }) => {
            assert_eq!(&generated_address.value, &address);
        }
        _ => panic!(
            "Expected an AddressNotFound for: {}",
            &generated_address.value
        ),
    };
}
