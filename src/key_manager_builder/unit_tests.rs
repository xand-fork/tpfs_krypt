use super::*;
#[cfg(feature = "hashicorp-vault")]
use crate::hashicorp_vault_key_manager::HashicorpVaultKeyManagerConfig;
use crate::{
    config::{KeyManagerConfig, KryptConfig},
    file_key_manager::FileKeyManagerConfig,
    in_memory_key_manager::InMemoryKeyManagerConfig,
};
use std::fs::write;
use tempfile::{tempdir, TempDir};
#[cfg(feature = "hashicorp-vault")]
use url::Url;

fn create_file_keymanager_config() -> (TempDir, KryptConfig) {
    let directory = tempdir().unwrap();

    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
            keypair_directory_path: directory.path().to_str().unwrap().to_string(),
        }),
    };

    (directory, config)
}

#[test]
fn valid_config() {
    let (_drop_me, config) = create_file_keymanager_config();

    let result = from_config(config);

    assert!(result.is_ok());
}

#[test]
fn invalid_empty_config() {
    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
            keypair_directory_path: String::from(""),
        }),
    };
    let result = from_config(config);

    assert!(result.is_err());
}

fn create_ok_config_file_keymanager_config_file() -> (TempDir, TempDir, String) {
    create_file_keymanager_config_file(format_ok_config)
}

fn format_ok_config(keypair_directory_path: String) -> String {
    format!(
        r#"
            [key_manager_config.FileKeyManager]
            keypair_directory_path = "{}"
        "#,
        keypair_directory_path
    )
}

fn create_file_keymanager_config_file<F>(config_formmater: F) -> (TempDir, TempDir, String)
where
    F: Fn(String) -> String,
{
    let keypair_directory = tempdir().unwrap();
    let config_string = config_formmater(keypair_directory.path().to_str().unwrap().to_string());
    let config_directory = tempdir().unwrap();
    let config_directory_string = config_directory.path().to_str().unwrap().to_string();
    let config_file_path = config_directory.path().join("krypt.toml");
    write(config_file_path, config_string).unwrap();

    (keypair_directory, config_directory, config_directory_string)
}

#[test]
fn valid_config_file() {
    let (_drop_me, _drop_me_too, config_path) = create_ok_config_file_keymanager_config_file();

    let result = from_config_path(&config_path);

    assert!(result.is_ok());
}

fn format_invalid_serde_config(keypair_directory_path: String) -> String {
    format!(
        r#"
            [key_manager_config]
            keypair_directory_path = "{}"
        "#,
        keypair_directory_path
    )
}

#[test]
fn invalid_serde_config_file() {
    let (_drop_me, _drop_me_too, config_path) =
        create_file_keymanager_config_file(format_invalid_serde_config);

    let result = from_config_path(&config_path);

    assert!(result.is_err());
}

#[test]
fn valid_in_memory_config() {
    let in_memory_config = InMemoryKeyManagerConfig {
        initial_keys: vec![],
    };
    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::InMemoryKeyManager(in_memory_config),
    };

    let result = from_config(config);

    assert!(result.is_ok());
}

#[test]
#[cfg(feature = "hashicorp-vault")]
fn invalid_vault_config() {
    let vault_key_config = HashicorpVaultKeyManagerConfig {
        vault_path: String::from("new/keypath/location/"),
        vault_token: String::from("sometoken"),
        vault_addr: Url::parse("http://127.0.0.1:3700").unwrap(),
    };
    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::HashicorpVaultKeyManager(vault_key_config),
    };

    let result = from_config(config);

    assert!(result.is_err());
}
