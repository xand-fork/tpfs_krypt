// use super::*;
// use test_case::test_case;
// use url::Url;
// TODO: For some reason these are trying to reach out to a real server so they are not unit tests

// fn create_vault_config(path: String) -> HashicorpVaultKeyManagerConfig {
//     let addr = std::env::var("VAULT_ADDR").unwrap_or_else(|_| "http://127.0.0.1:8200".into());
//     let addr = Url::parse(&addr).unwrap();
//     HashicorpVaultKeyManagerConfig {
//         vault_path: path,
//         vault_token: std::env::var("VAULT_TOKEN").unwrap_or_else(|_| "testtoken".into()),
//         vault_addr: addr,
//     }
// }

// #[test_case("test/path/", "test/path/" ; "No alterations needed.")]
// #[test_case("/test/path/", "test/path/" ; "When starting with a /.")]
// #[test_case("test/path", "test/path/" ; "Without ending with a /.")]
// #[test_case("/test/path", "test/path/" ; "When starting with a / and ending without one.")]
// fn test_path_correcting_behavior_on_new(path: &str, expected_path: &str) {
//     let config = create_vault_config(path.to_string());
//     let key_manager = HashicorpVaultKeyManager::new(config);

//     assert_eq!(expected_path.to_string(), key_manager.vault_path);
// }
