use crate::{core::KeyPair, crypto::translator::KeyPairInstance, unit_tests::prelude::*, KeyType};
use proptest::prelude::*;
use secrecy::{ExposeSecret, Secret};
use strum::IntoEnumIterator;

fn create_equivalent_translator(translator: &KeyPairInstance) -> KeyPairInstance {
    KeyPairInstance::from_strings(translator.to_secret().unwrap(), translator.get_key_type())
        .unwrap()
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn generates_are_different(key_type in keytype_strategy()) {
        let generate1 = KeyPairInstance::generate(key_type).unwrap();
        let generate2 = KeyPairInstance::generate(key_type).unwrap();

        prop_assert_ne!(generate1.identifier(), generate2.identifier());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn to_address_are_the_same(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();
        let copied = create_equivalent_translator(&generated);
        let generated_address = generated.identifier();
        let copied_address = copied.identifier();

        prop_assert_eq!(generated_address, copied_address);
        prop_assert_eq!(generated.to_address(), copied.to_address());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn to_secret_are_the_same(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();
        let copied = create_equivalent_translator(&generated);
        let generated_secret = generated.to_secret().unwrap();
        let copied_secret = copied.to_secret().unwrap();

        prop_assert_eq!(generated_secret.expose_secret(), copied_secret.expose_secret());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn new_via_generate(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();
        let secret_string = generated.to_secret().unwrap();

        let new_key = KeyPairInstance::from_secret(
            secret_string,
            key_type
        ).unwrap();

        prop_assert_eq!(generated.identifier(), new_key.identifier());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn from_string_via_generate(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();
        let from_strings = KeyPairInstance::from_strings(
            generated.to_secret().unwrap(),
            key_type
        ).unwrap();

        prop_assert_eq!(generated.identifier(), from_strings.identifier());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32 * DEV_DERIVED_KEY_COUNT))]
    #[test]
    fn from_string_via_dev_derived_keys((secret, path) in get_dev_derived_key_strategy(),
                                        key_type in sign_capable_keytype_strategy()) {
        let from_secret = KeyPairInstance::from_secret(
            Secret::new(secret.clone()),
            key_type
        );
        let from_secret = from_secret.unwrap();

        let paths = get_paths(&from_secret);
        prop_assert!(paths.is_some());

        // Verifies that the paths match from the underlying type.
        let paths = paths.unwrap();
        prop_assert_eq!(1, paths.len());
        prop_assert_eq!(&path, &paths[0]);

        let from_secret_string = from_secret.to_secret().unwrap();
        prop_assert_eq!(&&secret, &from_secret_string.expose_secret());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(DEV_DERIVED_KEY_COUNT))]
    #[test]
    fn from_string_check_sr25519_address((secret, path) in get_dev_derived_key_strategy()) {
        let from_strings = KeyPairInstance::from_secret(
            Secret::new(secret),
            KeyType::SubstrateSr25519,
        ).unwrap();

        prop_assert_eq!(DERIVED_KEY_SR25519_ADDRESS_LOOKUP.get(&path).unwrap().clone(), from_strings.identifier().value);
    }
}
