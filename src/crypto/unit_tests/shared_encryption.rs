use crate::{
    core::{KeyPair, SharedEncryptor},
    crypto::translator::KeyPairInstance,
    DecryptionStrategy::{Receiver, Sender},
    KeyType, SentMessage,
};
use proptest::prelude::*;
use secrecy::ExposeSecret;

proptest! {
    #![proptest_config(ProptestConfig::with_cases(20))]
    #[test]
    fn sender_encrypt_receiver_decrypt(
        message in proptest::string::bytes_regex(".+").unwrap(),
        public_input in proptest::string::bytes_regex(".+").unwrap()
    ) {
        let sender_key = KeyPairInstance::generate(KeyType::SharedEncryptionX25519).unwrap();
        let receiver_key = KeyPairInstance::generate(KeyType::SharedEncryptionX25519).unwrap();

        let receiver_public_key = &receiver_key.public();

        let encrypted = sender_key.shared_encrypt(receiver_public_key, &message,
                                                  &public_input).unwrap();
        let decrypted = receiver_key.shared_decrypt(Receiver(encrypted)).unwrap();
        prop_assert_eq!(message, decrypted.expose_secret().as_slice());
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(20))]
    #[test]
    fn sender_encrypt_decrypt(
        message in proptest::string::bytes_regex(".+").unwrap(),
        public_input in proptest::string::bytes_regex(".+").unwrap()
    ) {
        let sender_key = KeyPairInstance::generate(KeyType::SharedEncryptionX25519).unwrap();
        let receiver_key = KeyPairInstance::generate(KeyType::SharedEncryptionX25519).unwrap();
        let receiver_public_key = receiver_key.public();

        let encrypted = sender_key.shared_encrypt(&receiver_public_key, &message, &public_input)
            .unwrap();
        let mut receiver_pubkey = <[u8; 32]>::default();
        receiver_pubkey.clone_from_slice(receiver_public_key.as_slice());
        let sent_message = SentMessage {
            ephemeral_key_input: public_input.to_vec(),
            receiver_pubkey,
            ciphertext: encrypted.ciphertext
        };
        let decrypted = sender_key.shared_decrypt(Sender(sent_message))
            .unwrap();

        prop_assert_eq!(message, decrypted.expose_secret().as_slice());
    }
}
