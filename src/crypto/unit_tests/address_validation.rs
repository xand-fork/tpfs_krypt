use crate::{crypto::substrate::Address, errors::KeyManagementError, AddressValidation, KeyType};
use sp_core::crypto::PublicError;
use test_case::test_case;

#[allow(clippy::unused_unit)]
#[test_case(KeyType::SubstrateSr25519, "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ" ; "Valid 2Pac Address")]
#[test_case(KeyType::SubstrateSr25519, "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ" ; "Valid Biggie Address")]
fn test_valid_addresses(key_type: KeyType, address: &str) {
    let address = Address {
        value: address.into(),
        key_type,
    };

    let result = address.validate();

    assert!(result.is_ok());
}

#[allow(clippy::unused_unit)]
#[test_case(KeyType::SubstrateSr25519, "USNGANI", PublicError::BadBase58 ; "Sr25519 using an I which is not base58")]
#[test_case(KeyType::SubstrateEd25519, "USNGANI", PublicError::BadBase58 ; "Ed25519 Using an I which is not base58")]
#[test_case(KeyType::SubstrateSr25519, "The0", PublicError::BadBase58 ; "Using a 0 which is not base58")]
#[test_case(KeyType::SubstrateEd25519, "Thel", PublicError::BadBase58 ; "Using a l which is not base58")]
#[test_case(KeyType::SubstrateSr25519, "The.", PublicError::InvalidFormat ; "Using a period which is not alphanumeric")]
#[test_case(KeyType::SubstrateEd25519, "The/", PublicError::InvalidFormat ; "Using a forward-slash which is not alphanumeric")]
#[test_case(KeyType::SubstrateSr25519, "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqY", PublicError::BadLength ; "Using a short Address")]
#[test_case(KeyType::SubstrateEd25519, "FEgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ", PublicError::UnknownVersion ; "Using an unknown version")]
fn test_invalid_addresses(key_type: KeyType, address: &str, public_error: PublicError) {
    let address = Address {
        value: address.into(),
        key_type,
    };

    let result = address.validate();

    assert!(result.is_err());
    match result {
        Err(KeyManagementError::SubstrateSs58ConversionError {
            source,
            address: addr,
        }) => {
            assert_eq!(source.value, public_error);
            assert_eq!(addr, Some(address.value));
        }
        _ => panic!("Got an okay response which shouldn't be possible."),
    };
}
