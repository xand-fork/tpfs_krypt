use super::*;
use crate::errors::KeyManagementError;
use rand::SeedableRng;
use secrecy::{ExposeSecret, Secret};
use sp_core::{ed25519, Pair};

#[test]
fn test_encrypt_and_decrypt() {
    let mut test_rng = rand::rngs::StdRng::from_seed([0u8; 32]);
    let (key, _secret_phrase, _seed) = ed25519::Pair::generate_with_phrase(None);
    let public_key = key.public().0;
    let mut private_key = [0u8; 32];
    test_rng.fill_bytes(&mut private_key);
    let private_key = Secret::new(private_key);

    let shared_secret = create_shared_secret(&public_key.to_owned().into(), &private_key);

    let plaintext = b"ABC";
    let encrypted = encrypt_with_shared_secret(
        shared_secret.expose_secret().as_bytes(),
        plaintext,
        &mut test_rng,
    )
    .unwrap();
    let decrypted =
        decrypt_with_shared_secret(shared_secret.expose_secret().as_bytes(), &encrypted).unwrap();

    assert_eq!(plaintext, decrypted.expose_secret().as_slice());
}

#[test]
fn decrypt_empty_ciphertext_returns_error() {
    assert!(matches!(
        decrypt_with_shared_secret(&Default::default(), &[]),
        Err(KeyManagementError::RingUnspecifiedError { .. })
    ));
}
