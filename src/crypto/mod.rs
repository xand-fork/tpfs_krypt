pub(crate) mod shared_encryption;
pub(crate) mod substrate;
pub(crate) mod translator;

#[cfg(test)]
mod unit_tests;

use crate::{errors::Result, KeyIdentifier, KeyType};
use rand::{CryptoRng, RngCore};
use secrecy::Secret;
use shared_encryption::x25519::SharedEncryptionX25519KeyPair;
use substrate::{ed25519::SubstrateEd25519KeyPair, sr25519::SubstrateSr25519KeyPair};

/// The complete keypair material, including the secret information. Generally you don't want
/// to work with this in an app - but during testing or configuration you may need manual control.
#[derive(Clone, Debug)]
pub enum KeyPairValue {
    /// A substrate-compatible sr25519 keypair
    SubstrateSr25519(SubstrateSr25519KeyPair),
    /// A substrate-compatible ed25519 keypair
    SubstrateEd25519(SubstrateEd25519KeyPair),
    /// A keypair for performing shared encryption
    SharedEncryptionX25519(SharedEncryptionX25519KeyPair),
}

/// Being within the crypto module the RawKeyPair representation contains
/// similar methods as KeyPair within core.rs except that this will work
/// with the raw types via associated types. This is expected to be worked
/// with via static dispatch.
pub(crate) trait RawKeyPair: Sized {
    /// The exact types to be used with for Public Keys and Private Keys.
    type PublicKey: AsRef<[u8]>;
    type Signature: AsRef<[u8]>;
    const KEY_TYPE: KeyType;

    fn from_strings(secret: Secret<String>) -> Result<Self>;
    fn from_secret(secret: Secret<String>) -> Result<Self>;

    fn generate_with<R: RngCore + CryptoRng>(rng: &mut R) -> Result<Self>;

    fn generate() -> Result<Self> {
        Self::generate_with(&mut rand::thread_rng())
    }

    fn sign(&self, message: &[u8]) -> Result<Self::Signature>;

    fn identifier(&self) -> KeyIdentifier;

    fn to_secret(&self) -> Result<Secret<String>>;

    fn public(&self) -> Self::PublicKey;
    fn get_key_type(&self) -> KeyType {
        Self::KEY_TYPE
    }
}
