use super::*;

use tempfile::tempdir;

#[test]
fn valid_config_passes() {
    let directory = tempdir().unwrap();
    let path = String::from(directory.as_ref().to_str().unwrap());
    let manager_config = KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
        keypair_directory_path: path,
    });
    let krypt_config = KryptConfig::new(manager_config);

    let result = krypt_config.validate();

    assert!(result.is_ok());
}

/// Not intended to be exhaustive across all the different type of
/// config failures. Just makes sure that a single failure will
/// propogate to KryptConfig.
#[test]
fn single_invalid_failure() {
    let path = String::from("/foo/bar");
    let manager_config = KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
        keypair_directory_path: path,
    });
    let krypt_config = KryptConfig::new(manager_config);

    let result = krypt_config.validate();

    assert!(result.is_err());
}
