//! This module contains any non key manager specific configuration
//! for the `tpfs_krypt` crate.

#[cfg(feature = "hashicorp-vault")]
use crate::hashicorp_vault_key_manager::HashicorpVaultKeyManagerConfig;
use crate::{
    errors::Result, file_key_manager::FileKeyManagerConfig,
    in_memory_key_manager::InMemoryKeyManagerConfig,
};

#[cfg(test)]
mod unit_tests;

/// The Krypt Configuration that will contain the KeyManagerConfig
/// which will determine which KeyManager to create.
#[derive(Constructor, Debug, Deserialize, Eq, PartialEq, Serialize, Clone)]
pub struct KryptConfig {
    /// The enum that contains which key manager to
    /// create.
    pub key_manager_config: KeyManagerConfig,
}

/// An enum that defines the different types of KeyManagers that
/// are able to be created. This will also contain the configuration
/// pertinent to the KeyManager that will be created.
#[derive(Debug, Deserialize, Eq, PartialEq, Serialize, Clone)]
pub enum KeyManagerConfig {
    /// Create a FileKeyManager to manage keys
    /// via a file path.
    FileKeyManager(FileKeyManagerConfig),
    /// Create a InMemoryKeyManager to manage keys
    /// only in memory.
    InMemoryKeyManager(InMemoryKeyManagerConfig),
    /// Create a Hashicorp Vault KeyManager to manage keys
    /// to a remote connection of vault.
    #[cfg(feature = "hashicorp-vault")]
    HashicorpVaultKeyManager(HashicorpVaultKeyManagerConfig),
}

/// The ability to validate the configuration using this trait.
pub trait ConfigValidation {
    /// Performs the validation and returns any issues
    /// as an Err on the result. This will only return
    /// the first error that was encountered.
    fn validate(&self) -> Result<()>;
}

impl ConfigValidation for KryptConfig {
    fn validate(&self) -> Result<()> {
        match &self.key_manager_config {
            KeyManagerConfig::FileKeyManager(config) => config.validate(),
            KeyManagerConfig::InMemoryKeyManager(config) => config.validate(),
            #[cfg(feature = "hashicorp-vault")]
            KeyManagerConfig::HashicorpVaultKeyManager(config) => config.validate(),
        }
    }
}
