use crate::{
    errors::KeyManagementError,
    in_memory_key_manager::{InMemoryKeyManager, InMemoryKeyManagerConfig},
    unit_tests::prelude::*,
    DecryptionStrategy::{Receiver, Sender},
    KeyIdentifier, KeyManagement, KeyType, SentMessage, SharedEncryption, Signing,
};
use proptest::prelude::*;
use secrecy::ExposeSecret;
use strum::IntoEnumIterator;

// Tests in this module/file are intended to test out the code in core.rs around `KeyStorage` in conjunction with a `KeyManagement`

fn create_in_memory_key_manager() -> InMemoryKeyManager {
    let config = InMemoryKeyManagerConfig {
        initial_keys: vec![],
    };

    InMemoryKeyManager::new(config)
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn perform_shared_encryption_with_generated_keys(
        key_type in keytype_strategy(),
        message in proptest::string::bytes_regex(".+").unwrap()
    ) {
        let mut sender_key_manager = create_in_memory_key_manager();
        let sender = sender_key_manager.generate_keypair(key_type).unwrap();
        let mut receiver_key_manager = create_in_memory_key_manager();
        let receiver = receiver_key_manager.generate_keypair(key_type).unwrap();

        let key_and_encrypted =
            sender_key_manager.shared_encrypt(sender.as_ref(), &receiver.pubkey,
                                                           &message, &[]);
        // Skip types that don't support shared encryption.
        if let Err(KeyManagementError::SharedEncryptionNotSupported { key_type: _ }) = key_and_encrypted {
            return Ok(());
        }
        let encrypted = key_and_encrypted.unwrap();

        let decrypted = receiver_key_manager.shared_decrypt(receiver.as_ref(), Receiver(encrypted.clone())).unwrap();

        prop_assert_eq!(message.as_slice(), decrypted.expose_secret().as_slice());
        let mut receiver_pubkey = <[u8; 32]>::default();
        receiver_pubkey.clone_from_slice(receiver.pubkey.as_slice());
        let sent_message = SentMessage {
            ephemeral_key_input: vec![],
            receiver_pubkey,
            ciphertext: encrypted.ciphertext.to_vec(),
        };
        let sender_decrypted = sender_key_manager.shared_decrypt(sender.as_ref(), Sender(sent_message)).unwrap();

        prop_assert_eq!(message.as_slice(), sender_decrypted.expose_secret().as_slice());
    }
}

proptest! {
    // Doesn't actually verify the signature since those tests
    // exist in the crypto module.
    #![proptest_config(ProptestConfig::with_cases(20))]
    #[test]
    fn signing_occurs_from_key_manager_interface(
        key_type in sign_capable_keytype_strategy(),
        message in proptest::string::bytes_regex(".+").unwrap()
    ) {
        let mut sut = create_in_memory_key_manager();
        let generated_address = sut.generate_keypair(key_type).unwrap();

        let signed = sut.sign(generated_address.as_ref(), message.as_slice()).unwrap();

        prop_assert_eq!(!signed.as_ref().as_ref().is_empty(), true);
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(KeyType::iter().len() as u32))]
    #[test]
    fn sign_invalid_address(key_type in keytype_strategy()) {
        let sut = create_in_memory_key_manager();
        let invalid_address = KeyIdentifier {
            value: "../../5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ".into(),
            key_type
        };

        let result = sut.sign(&invalid_address, b"Some Message");

        prop_assert!(result.is_err());
    }
}
