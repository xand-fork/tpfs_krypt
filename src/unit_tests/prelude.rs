use crate::{
    crypto::{translator::KeyPairInstance, KeyPairValue},
    KeyType,
};
use proptest::prelude::*;
use secrecy::ExposeSecret;
use sp_core::crypto::DEV_PHRASE;
use std::collections::HashMap;
use strum::IntoEnumIterator;

pub(crate) fn sign_capable_keytype_strategy() -> BoxedStrategy<KeyType> {
    let justs: Vec<Just<KeyType>> = KeyType::iter()
        // Shared encryption does not support signing
        .filter(|x| *x != KeyType::SharedEncryptionX25519)
        .map(Just)
        .collect();
    union_justs(justs)
}

pub(crate) fn keytype_strategy() -> BoxedStrategy<KeyType> {
    let justs: Vec<Just<KeyType>> = KeyType::iter().map(Just).collect();
    union_justs(justs)
}

fn union_justs(justs: Vec<Just<KeyType>>) -> BoxedStrategy<KeyType> {
    if justs.len() > 1 {
        let mut strategy = justs[0].prop_union(justs[1]);
        for just in justs.iter().skip(2) {
            strategy = strategy.or(*just);
        }
        return strategy.boxed();
    }
    panic!("There should be more than 1 KeyType.");
}

pub(crate) const DEV_DERIVED_KEY_COUNT: u32 = 4;

lazy_static! {
    pub(crate) static ref DERIVED_KEY_SR25519_ADDRESS_LOOKUP: HashMap<String, String> = vec![
        (
            "/Biggie",
            "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ"
        ),
        ("/2Pac", "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"),
        ("/Xand", "5Cqm7KKdm8MB7jR66mxKKcUAzKGFbZnYJqMvQQBpgC5P9C2W"),
        ("/Trust", "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7")
    ]
    .into_iter()
    .map(|(path, address)| (String::from(path), String::from(address)))
    .collect();
}

pub(crate) fn get_dev_derived_key_strategy() -> BoxedStrategy<(String, String)> {
    let derived_paths = vec!["/Biggie", "/2Pac", "/Xand", "/Trust"];
    let justs: Vec<Just<(String, String)>> = derived_paths
        .into_iter()
        .map(String::from)
        .map(|path| Just((format!("{}/{}", DEV_PHRASE, path), path)))
        .collect();

    if justs.len() > 1 {
        let mut strategy = justs[0].clone().prop_union(justs[1].clone());
        for just in justs.iter().skip(2) {
            strategy = strategy.or(just.clone());
        }
        return strategy.boxed();
    }

    panic!("There should be more than 1 Derived Path.");
}

pub(crate) fn get_paths(keypair: &KeyPairInstance) -> Option<Vec<String>> {
    let paths = match &keypair.0 {
        KeyPairValue::SubstrateSr25519(kp) => Some(&kp.derive_paths),
        KeyPairValue::SubstrateEd25519(kp) => Some(&kp.derive_paths),
        KeyPairValue::SharedEncryptionX25519(_) => None,
    };

    paths.map(|paths| {
        paths
            .iter()
            .map(|path| path.expose_secret().clone())
            .collect()
    })
}
