use crate::unit_tests::prelude::*;
use crate::{
    core::KeyPair,
    crypto::translator::KeyPairInstance,
    persistence::{deserialize_keypair, serialize_keypair},
};
use proptest::prelude::*;
use secrecy::ExposeSecret;
use std::string::ToString;
const SERIALIZATION_CASES: u32 = 20;

proptest! {
    #![proptest_config(ProptestConfig::with_cases(SERIALIZATION_CASES))]
    #[test]
    fn has_associated_data_in_serialization(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();

        let serialized = serialize_keypair(&generated).unwrap();
        let contents = serialized.expose_secret();

        prop_assert!(contents.contains(&key_type.to_string()));
        prop_assert!(contents.contains(&generated.identifier().value));
        prop_assert!(contents.contains(generated.to_secret().unwrap().expose_secret()));
    }
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(SERIALIZATION_CASES))]
    #[test]
    fn has_same_data_when_deserialized(key_type in keytype_strategy()) {
        let generated = KeyPairInstance::generate(key_type).unwrap();

        let serialized = serialize_keypair(&generated).unwrap();
        let deserialized = deserialize_keypair(serialized).unwrap();

        prop_assert_eq!(generated.get_key_type(), deserialized.get_key_type());
        prop_assert_eq!(&generated.identifier().value, &deserialized.identifier().value);

        let generated_secret_string = generated.to_secret().unwrap();
        let deserialized_secret_string = deserialized.to_secret().unwrap();
        prop_assert_eq!(&generated_secret_string.expose_secret(), &deserialized_secret_string.expose_secret());
    }
}
