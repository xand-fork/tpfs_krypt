#![forbid(unsafe_code)]
#![deny(missing_docs)]
#![deny(missing_debug_implementations)]
#![cfg_attr(test, deny(warnings))]
#![doc(
    html_root_url = "https://transparentincdevelopment.gitlab.io/product/libs/tpfs_krypt/tpfs_krypt"
)]

//! # Tpfs Krypt
//!
//! The `tpfs_krypt` crate provides an implementation-agnostic interface used to manage
//! (e.g. generate, list or import keypairs) and work with (e.g. sign and later
//! encrypt) cryptographic secrets.
//!
//! The common apis are [`KeyManagement`] and [`Signature`] traits, and the [`Address`] struct.
//!
//! [`KeyManagement`]: trait.KeyManagement.html
//! [`Signature`]: trait.Signature.html
//! [`Address`]: struct.Address.html
//!
//! How to configure a specific KeyManager can be found within the module. Two sample ones are
//! the [`file_key_manager`] and the [`in_memory_key_manager`] modules.
//!
//! [`file_key_manager`]: file_key_manager/index.html
//! [`in_memory_key_manager`]: in_memory_key_manager/index.html
//!
//! ## Example: Performing Signing
//!
//! Here's a basic example of using a directory and the FileKeyManager to create a key and then perform a signing.
//!
//! ```rust
//! use std::fs;
//! use std::path::PathBuf;
//! use tpfs_krypt::{
//!     config::{KeyManagerConfig, KryptConfig},
//!     errors::KeyManagementError,
//!     from_config, FileKeyManagerConfig, KeyType
//! };
//!
//! let path = PathBuf::from("/tmp/krypt/keypairs/");
//! if !path.exists() {
//!     fs::create_dir_all(&path).unwrap();
//! }
//!
//! let config = KryptConfig {
//!     key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
//!         keypair_directory_path: path.into_os_string().into_string().unwrap(),
//!     }),
//! };
//!
//! let mut key_manager = from_config(config)?;
//! let address = key_manager.generate_keypair(KeyType::SubstrateSr25519)?;
//! let signature = key_manager.sign(address.as_ref(), b"My important message that can be verified was done by me via my public address.")?;
//! let signature_bytes: &[u8] = signature.as_ref().as_ref();
//! # Ok::<(), KeyManagementError>(())
//! ```
//!
//! ## Example: Performing shared encryption, generating a deterministic key per-message.
//!
//! Shared Encryption allows for two parties to share a secret with each other. Diffie-Hellman is
//! used for this purpose, but we deviate from the standard interactive protocol in order to avoid
//! additional storage overhead in the context of on-chain transactions. Rather than having the
//! initiator send some encrypted randomness to the receiver, we use something that is unique and
//! publicly available (as part of the transaction, typically) to take the place of this randomness.
//! This also makes it convenient for the initiator/sender to decrypt old messages, since the
//! process is always the same. IE: They don't have to store the randomness that would normally
//! be transmitted to the receiver somewhere.
//!
//! This does mean that, if either party's private key is stolen, the owner will be able to decrypt
//! the exchanged data. Periodic rotation of encryption keys is recommended.
//!
//! ```rust
//! use secrecy::ExposeSecret;
//! use std::fs;
//! use std::path::PathBuf;
//! use tpfs_krypt::{
//!     config::{KeyManagerConfig, KryptConfig},
//!     errors::KeyManagementError,
//!     from_config, FileKeyManagerConfig, KeyManagement, KeyType, SharedEncryption, DecryptionStrategy
//! };
//!
//! fn create_file_key_manager(path: PathBuf) -> Result<Box<dyn KeyManagement>, KeyManagementError> {
//!     if !path.exists() {
//!         fs::create_dir_all(&path).unwrap();
//!     }
//!
//!     let config = KryptConfig {
//!         key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
//!             keypair_directory_path: path.into_os_string().into_string().unwrap(),
//!         }),
//!     };
//!
//!     from_config(config)
//! }
//!
//! let mut sender_key_manager = create_file_key_manager(PathBuf::from("/tmp/krypt/sender/keypairs/"))?;
//! let mut receiver_key_manager = create_file_key_manager(PathBuf::from("/tmp/krypt/receiver/keypairs/"))?;
//! let sender = sender_key_manager.generate_keypair(KeyType::SharedEncryptionX25519)?;
//! let receiver = receiver_key_manager.generate_keypair(KeyType::SharedEncryptionX25519)?;
//!
//! let message = b"My super super secret message.";
//! let public_input = b"Some public data";
//! let encrypted = sender_key_manager.shared_encrypt(sender.as_ref(),
//!                                                                &receiver.pubkey,
//!                                                                message,
//!                                                                public_input)?;
//! let decrypted = receiver_key_manager.shared_decrypt(receiver.as_ref(),
//!                                                     DecryptionStrategy::Receiver(encrypted))?;
//! assert_eq!(message, decrypted.expose_secret().as_slice());
//! # Ok::<(), KeyManagementError>(())
//! ```
//!
//! ## Substrate DEV_PHRASE with derived keys.
//!
//! You'll notice the use of the `Secrecy` crate and that it is to make sure that the phrase's
//! contents are zeroed out from memory once the secret is imported into the key manager.
//! The `sp_core` crate is used for pulling in substrate keys and for checking the address
//! returned from the key manager match.
//!
//! ```rust
//! use std::{path::PathBuf, fs};
//! use tpfs_krypt::{
//!     config::{KeyManagerConfig, KryptConfig},
//!     errors::KeyManagementError,
//!     from_config, FileKeyManagerConfig, KeyType, secrecy::Secret,
//!     sp_core::{crypto::{DEV_PHRASE, Pair, Ss58Codec}, sr25519}
//! };
//!
//! let path = PathBuf::from("/tmp/krypt/keypairs/");
//! if !path.exists() {
//!     fs::create_dir_all(&path).unwrap();
//! }
//!
//! let config = KryptConfig {
//!     key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
//!         keypair_directory_path: path.into_os_string().into_string().unwrap(),
//!     }),
//! };
//!
//! let mut key_manager = from_config(config)?;
//! let secret_phrase = Secret::new(format!("{}//Xand", DEV_PHRASE));
//! let address = key_manager.import_keypair(secret_phrase, KeyType::SubstrateSr25519)?;
//! let signature = key_manager.sign(address.as_ref(), b"My important message that can be verified was done by me via my public address.")?;
//! let signature_bytes: &[u8] = signature.as_ref().as_ref();
//!
//! // You can generate the address yourself with the code below.
//! let pair = sr25519::Pair::from_string("//Xand", None)?;
//! assert_eq!(pair.public().to_ss58check(), address.id.value);
//! # Ok::<(), KeyManagementError>(())
//! ```
//!
//! ## Example: For use in Tests
//!
//! Since it can be kind of heavy to set up a directory with keys to just run a series of tests.
//! This is what the InMemoryKeyManager was built for to allow for quick uses of the key manager
//! without having to set up a way to actually persist the keys. It allows for importing a set of
//! initial keys to avoid having to run import and manage them as secrets.
//!
//! ```rust
//! use sp_core::crypto::DEV_PHRASE;
//! use tpfs_krypt::{
//!     config::{KeyManagerConfig, KryptConfig},
//!     from_config, InMemoryKeyManagerConfig, KeyType,
//! };
//!
//! let config = KryptConfig {
//!     key_manager_config: KeyManagerConfig::InMemoryKeyManager(InMemoryKeyManagerConfig {
//!         initial_keys: get_initial_keys(),
//!     }),
//! };
//!
//! let key_manager = from_config(config).unwrap();
//!
//! /// Creates an initial set of keys for tests to work with.
//! pub fn get_initial_keys() -> Vec<(KeyType, String)> {
//!     let paths = vec!["Biggie", "2Pac", "Xand", "Trust"];
//!
//!     let mut keys: Vec<(KeyType, String)> = paths
//!         .iter()
//!         .map(|path| {
//!             (
//!                 KeyType::SubstrateSr25519,
//!                 format!("{}//{}", DEV_PHRASE, path),
//!             )
//!         })
//!         .collect();
//!
//!     keys.push((KeyType::SubstrateSr25519, String::from(DEV_PHRASE)));
//!     keys
//! }
//! ```

extern crate config as cfg;
#[macro_use]
extern crate derive_more;
#[macro_use]
extern crate lazy_static;
#[cfg(test)]
extern crate proptest;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate strum_macros;

#[cfg(feature = "hashicorp-vault")]
pub use self::hashicorp_vault_key_manager::HashicorpVaultKeyManagerConfig;
pub use self::{
    file_key_manager::FileKeyManagerConfig,
    in_memory_key_manager::InMemoryKeyManagerConfig,
    key_manager_builder::{from_config, from_config_path},
    persistence::KEYPAIR_FILE_PREFIX,
};
pub use crate::core::{KeyPair, Signer};
pub use crypto::{translator::KeyPairInstance, KeyPairValue};
pub use secrecy;
pub use sp_core;
pub use typed::Encrypted;
pub use xand_key_pair::{
    AnyXandKeyPair, LimitedAgentKeyPair, TrustKeyPair, ValidatorAuthorityKeyPair,
    ValidatorLibp2pKeyPair, ValidatorSessionFinalizeBlocksKeyPair,
    ValidatorSessionProduceBlocksKeyPair, XandKeyPair, XandKeyType,
};

pub mod config;
pub(crate) mod core;
pub(crate) mod crypto;
pub mod errors;
pub mod file_key_manager;
#[cfg(feature = "hashicorp-vault")]
pub mod hashicorp_vault_key_manager;
pub mod in_memory_key_manager;
mod key_manager_builder;
mod persistence;
pub mod typed;
#[cfg(test)]
mod unit_tests;
mod xand_key_pair;

use errors::Result;
use secrecy::Secret;
use std::convert::TryFrom;
use zeroize::Zeroize;

/// The different sorts of keys that will be managed via this crate.
/// This will be persisted with the key for when we may want to be able
/// to return the type of the key after it's been imported or generated.
#[derive(
    Clone,
    Copy,
    Debug,
    Deserialize,
    Display,
    EnumString,
    EnumIter,
    Eq,
    Hash,
    PartialEq,
    Serialize,
    Zeroize,
)]
pub enum KeyType {
    /// A key of type sr25519 that also is used based on substrate details like signing context, and
    /// using bip39 mnemonics for a secret phrase.
    SubstrateSr25519,
    /// A key of type ed25519 that also is used based on substrate details like using bip39
    /// mnemonics for a secret phrase.
    SubstrateEd25519,
    /// This key type is for keys that will be used to encrypt data between two parties. It uses
    /// Curve 25519 to create an asymmetric keys which can then be used with a private key from one
    /// party with the public key of the other party to create a symmetric key, which can be
    /// reproduced by the other party. Operations like signing and path derivation are unsupported.
    SharedEncryptionX25519,
}

/// The ability to validate the address using this trait.
pub trait AddressValidation {
    /// Performs the validation and returns any issues
    /// as an Err on the result. This will only return
    /// the first error that was encountered.
    fn validate(&self) -> Result<()>;
}

/// Uniquely identifies (the public portion of) a key
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct KeyIdentifier {
    /// String representation of the key ID
    pub value: String,
    /// The key_type to keep track of what kind of key
    /// the address relates to.
    pub key_type: KeyType,
}

/// Returned specifically from calls to generate new keypairs. Includes the public key bytes, which
/// is useful for shared encryption, for example.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct NewKeyId {
    /// The key identifier
    pub id: KeyIdentifier,
    /// The public key bytes
    pub pubkey: Vec<u8>,
}

impl AsRef<KeyIdentifier> for NewKeyId {
    fn as_ref(&self) -> &KeyIdentifier {
        &self.id
    }
}

/// The trait for KeyManagement that is organized based on addresses
/// created based on the key type.
pub trait KeyManagement: Send + Sync + SharedEncryption + Signing {
    /// Retrieves all the key ids currently stored in this key manager
    fn get_key_ids(&self) -> Result<Vec<KeyIdentifier>>;

    /// A quick look up to see if the KeyManager has this particular key
    fn has_key(&self, key_id: &KeyIdentifier) -> Result<bool>;

    /// Generates a new keypair based on the KeyType supplied.
    fn generate_keypair(&mut self, key_type: KeyType) -> Result<NewKeyId>;

    /// Allows for saving a keypair that may have been created outside of the KeyManager.
    fn import_keypair(&mut self, secret: Secret<String>, key_type: KeyType) -> Result<NewKeyId>;

    /// Get all the full keypair including the secret
    fn get_keypair_by_id(&self, key_id: &KeyIdentifier) -> Result<KeyPairValue>;
}

/// A trait that just performs encryption that is intended to be both ways via SharedEncryption
/// which is accessible via KeyManagement trait.
pub trait SharedEncryption: Send + Sync {
    /// Creates a shared key which can be recreated from the receiver's private key and the public
    /// key resulting from generating a new key. Then encrypts the message with that shared key.
    ///
    /// Generates a key at the time of encrypting and includes the newly created public key in the
    /// result. This key is deterministic as a function of `ephemeral_key_input` - so callers of
    /// this function must ensure that `ephemeral_key_input` is also known/determinable by the
    /// decrypter.
    ///
    /// The returned key may be safely discarded as long as `ephemeral_key_input` can always be
    /// determined if the need arises to decrypt the data later. If there is no such need, neither
    /// need be remembered.
    fn shared_encrypt(
        &self,
        your_key_id: &KeyIdentifier,
        other_pubkey: &[u8],
        message: &[u8],
        ephemeral_key_input: &[u8],
    ) -> Result<EncryptedMessage>;

    /// Performs decryption of the cipher value to the original message bytes.
    ///
    /// Recreating the shared key is dependent on `ephemeral_key_input` being identical to what
    /// was provided to `shared_encrypt` when the message was encrypted.
    fn shared_decrypt(
        &self,
        your_key_id: &KeyIdentifier,
        decryption_strategy: DecryptionStrategy,
    ) -> Result<Secret<Vec<u8>>>;
}

/// The decryption strategy provides the context needed to decrypt a message
/// based on whether you are the sender or receiver of the message,
/// as they are slightly different protocols.
#[derive(Clone, Debug)]
pub enum DecryptionStrategy {
    /// Used by the sender of an encrypted message to decrypt what they previously sent
    Sender(SentMessage),
    /// Used by the receiver of an encrypted message to decrypt
    Receiver(EncryptedMessage),
}

/// Contains an encrypted message sent by you,
/// along with the parameters used to generate the original encrypted message.
#[derive(Clone, Debug)]
pub struct SentMessage {
    /// The deterministic input used to generate the sender dh key for the message
    pub ephemeral_key_input: Vec<u8>,
    /// The Diffie-Hellman public key of the receiver
    pub receiver_pubkey: PublicKey,
    /// The encrypted message
    pub ciphertext: Vec<u8>,
}

/// Contains an encrypted message along with the sender Diffie-Hellman key.
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct EncryptedMessage {
    /// The Diffie-Hellman public key used for sending this message
    pub sender_key: PublicKey,
    /// The encrypted message
    pub ciphertext: Vec<u8>,
}

impl EncryptedMessage {
    /// Converts the encrypted message to bytes
    pub fn to_bytes(&self) -> Vec<u8> {
        bincode::serialize(self).expect("EncryptedMessage can be serialized to bytes")
    }

    /// Creates an encrypted message from bytes
    pub fn from_bytes(bytes: &[u8]) -> Result<Self> {
        bincode::deserialize(bytes).map_err(|e| {
            let message = format!("Failed to deserialize EncryptedMessage from bytes: {}", e);
            errors::KeyManagementError::ByteConversionError { message }
        })
    }
}

impl TryFrom<&[u8]> for EncryptedMessage {
    type Error = errors::KeyManagementError;

    fn try_from(bytes: &[u8]) -> Result<Self> {
        EncryptedMessage::from_bytes(bytes)
    }
}

/// A fixed sized type alias for a public key
pub type PublicKey = [u8; 32];

/// A trait for the signature that may be expanded on later.
pub trait Signature: AsRef<[u8]> {}

/// A trait that specifies any of the signing portions which is shared publicly via KeyManagement trait.
pub trait Signing: Send + Sync {
    /// Signs the message using the keypair associated with the provided address, if found.
    fn sign(&self, address: &KeyIdentifier, message: &[u8]) -> Result<Box<dyn Signature>>;
}

#[cfg(test)]
mod tests {
    use super::EncryptedMessage;

    #[test]
    fn encrypted_message_to_bytes_roundtrips() {
        let original = EncryptedMessage {
            sender_key: [22; 32],
            ciphertext: (0..4).collect(),
        };
        let serialized = original.to_bytes();
        let deserialized = EncryptedMessage::from_bytes(&serialized).unwrap();
        assert_eq!(deserialized, original);
    }

    #[test]
    fn encrypted_message_fails_to_deserialize_from_invalid_bytes() {
        let bytes = [0, 1, 2];
        assert!(matches!(
            EncryptedMessage::from_bytes(&bytes),
            Err(super::errors::KeyManagementError::ByteConversionError { .. })
        ));
    }
}
